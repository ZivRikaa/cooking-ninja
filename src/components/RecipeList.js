import './RecipeList.css'
import { Link } from 'react-router-dom'
import { useTheme } from '../hooks/UseTheme'
import Delet from '../assets/delet.svg'
import { projectFirestore } from '../firebase/config'
import React, { PureComponent } from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';




export default function RecipeList({ recipes }) { 
         
    const { mode } = useTheme()

    if (recipes.length === 0) {
        return <div className="error">No recipes to load...</div>
    }

    const handleClick = (id) => {
        projectFirestore.collection('recipes').doc(id).delete()
    }
    const data = recipes.map((recipe)=> {
        return{
          name: recipe.title,
          cookingTime: Number(recipe.CookingTime.split(' ')[0]),
        }
    })
    

    return <>
        <div className="recipe-list">
            {recipes.map(recipe => (
                <div key={recipe.id} className={` card ${mode}`}>
                    <h3>{recipe.title}</h3>
                    <p>{recipe.CookingTime} to make.</p>
                    <div>{recipe.method.substring(0, 100)}...</div>
                    <Link to={`/recipe/${recipe.id}`}>Cook This</Link>
                    {/* <Recipe theRecipe={recipe} /> */}
                    <img
                    className="delete"
                    src={Delet}
                    onClick={() => handleClick(recipe.id)}
                    alt="delet"
                    />
                </div>
            ))}
        </div>
        <div style={{
            height: '20em',
            width: '90%'
        }}>

    <ResponsiveContainer width="100%" height="100%">
        <BarChart
          width={1000}
          height={700}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="cookingTime" fill="#8884d8" name="cooking time" />
        </BarChart>
      </ResponsiveContainer>
      </div>
    </>
}

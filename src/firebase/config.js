import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyAg-V5BH74ggCbxtOmmDGBckxKSR39ZtPQ",
  authDomain: "cooking-ninja-site-d741a.firebaseapp.com",
  projectId: "cooking-ninja-site-d741a",
  storageBucket: "cooking-ninja-site-d741a.appspot.com",
  messagingSenderId: "115969329766",
  appId: "1:115969329766:web:1579e887e8cc5927f77ba7"
};

firebase.initializeApp(firebaseConfig)
const projectFirestore = firebase.firestore()
export { projectFirestore }



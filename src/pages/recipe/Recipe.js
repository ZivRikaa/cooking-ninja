import './Recipe.css'
import { useParams } from 'react-router-dom'
import { useTheme } from '../../hooks/UseTheme'
import { useEffect, useState } from 'react'
import { projectFirestore } from '../../firebase/config'


export default function Recipe({ theRecipe }) {
    const { id } = useParams()
    const { mode } = useTheme()
    
    const [recipe, setRecipe] = useState(null)
    const [isPending, setIsPending] = useState(false)
    const [error, setError] = useState(false) 

    useEffect(() => {
        setIsPending(true)
        projectFirestore.collection('recipes').doc(id).get().then((doc)=> {
            if(doc.exists) {
                setIsPending(false)
                setRecipe(doc.data())
            } else {
                setIsPending(false)
                setError("data doesn't exists")
            }
            
        })
    }, [id])

    const handleClick = () => {
        projectFirestore.collection('recipes').doc(id).update({
            title: 'somthing different'
        })
    }

    return (
        <div className={`recipe ${mode}`}>
            {isPending && <p className="loading">Loading...</p>}
            {error && <p className="error">{error}</p>}
            {recipe && (
                <>
                    <h2 className="page-title">{recipe.title}</h2>
                    <p>Takes {recipe.CookingTime} to cook.</p>
                    <ul>
                        {recipe.ingredients.map(ing => <li key={ing}>{ing}</li>)}
                    </ul>
                    <p className="method">{recipe.method}</p>
                    <button onClick={handleClick}>Update me</button>
                </>
            )}
        </div>
    )
}

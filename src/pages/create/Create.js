import { useState, useRef, useEffect } from 'react'
import './Create.css'
import { useHistory } from 'react-router-dom'
import { projectFirestore } from '../../firebase/config'
import toast from 'react-hot-toast'


export default function Create() {
    const [title, setTitle] = useState('')
    const [method, setMethod] = useState('')
    const [CookingTime, setCookingTime] = useState('')
    const [newIngredient, setNewIngrediet] = useState('')
    const [ingredients, setIngredients] = useState([])
    const ingredientsInput = useRef(null)
    const history = useHistory()


    const handleSubmit  = async (e) => {
        e.preventDefault()
        const doc = {title, ingredients, method, CookingTime: CookingTime + 'minutes'}
        
        try {
            toast.promise(
                projectFirestore.collection('recipes').add(doc),
                 {
                   loading: 'Creating...',
                   success: 'Created recipe',
                 }
               );
            // await projectFirestore.collection('recipes').add(doc)
            // toast.success('Created recipe')
            history.push('/')
        } catch (err) {
            console.log(err);
        }
    }

    const handleAdd = (e) => {
        e.preventDefault()
        const ing = newIngredient.trim()

        if (ing && !ingredients.includes(ing)) {
            setIngredients(prevInsredients => [...prevInsredients, ing])
        }
        setNewIngrediet('')        
        ingredientsInput.current.focus()
    }

    return (
        <div className="create">
            <h2 className="page-title">Add a new recipe</h2>
            <form onSubmit={handleSubmit}>

                <label>
                    <span>Recipe title:</span>
                    <input 
                        type="text"
                        onChange={(e) => setTitle(e.target.value)}
                        value={title}
                        required
                        />
                </label>

                <label>                   <span>Recipe ingredients:</span>
                    <div className="ingredients">
                        <input type="text"
                        onChange={(e) => setNewIngrediet(e.target.value)}
                        value={newIngredient}
                        ref={ingredientsInput}
                        />
                        <button className="btn" onClick={handleAdd}>add</button>
                    </div>
                </label>
                <p>Current Ingredients: {ingredients.map(i => <em key={i}>{i}, </em>)}</p>

                <label>
                    <span>Recipe method:</span>
                    <textarea 
                    onChange={(e) => setMethod(e.target.value)} 
                    value={method}
                    required
                    />
                </label>

                <label>
                    <span>Cooking time (minutes):</span>
                    <input 
                        type="number"
                        onChange={(e) => setCookingTime(e.target.value)}
                        value={CookingTime}
                        required
                    />
                </label>

                <button className="btn">submit</button>

            </form>
        </div>
    )
}
